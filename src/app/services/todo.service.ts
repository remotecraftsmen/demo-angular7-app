import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Todo} from '@app/models/todo';
import {environment} from '@env/environment';

@Injectable({
    providedIn: 'root'
})
export class TodoService {
    private apiURL = environment.backendUrl;

    constructor(private api: HttpClient) {
    }

    getAll() {
        return this.api.get(`${this.apiURL}/todos`);
    }

    create(todo: Todo) {
        return this.api.post(`${this.apiURL}/todos`, todo);
    }

    get(id: number) {
        return this.api.get(`${this.apiURL}/todos/${id}`);
    }

    update(id: number, data: {}) {
        return this.api.patch(`${this.apiURL}/todos/${id}`, data, {responseType: 'text'});
    }
}
