import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment';

@Injectable({
    providedIn: 'root'
})
export class ChangePasswordService {
    private apiURL = environment.backendUrl;

    constructor(private http: HttpClient) {
    }

    forgotPassword(email: string) {
        return this.http.post(`${this.apiURL}/reset-password`, {email}, {responseType: 'text'});
    }

    changePassword(token: string, password: string, password_confirmation: string) {
        return this.http.post(`${this.apiURL}/reset-password/${token}`, {password, password_confirmation});
    }
}
