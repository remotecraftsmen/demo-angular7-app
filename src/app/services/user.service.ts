import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '@app/models/user';
import {environment} from '@env/environment';
import {AuthService} from '@app/services/auth.service';
import {map} from 'rxjs/operators';
import {Subject} from 'rxjs';

@Injectable()
export class UserService {
    private updater: Subject<User>;
    private apiURL = environment.backendUrl;


    constructor(private api: HttpClient, private authService: AuthService) {
        this.updater = authService.getUpdater();
    }

    create(user: User) {
        return this.api.post<any>(this.apiURL + '/users', user)
            .pipe(
                map(data => {
                    localStorage.setItem('currentUser', JSON.stringify(data.user));
                    localStorage.setItem('token', data.token);
                    this.updater.next(data.user);

                    return data;
                })
            );
    }
}
