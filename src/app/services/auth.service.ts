import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {environment} from '@env/environment';
import {Observable, Subject} from 'rxjs';
import {map} from 'rxjs/operators';
import {User} from '@app/models/user';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    private apiURL = environment.backendUrl;
    private updater = new Subject<User>();

    constructor(private api: HttpClient) {
    }

    isUserUpdated(): Observable<User> {
        return this.updater.asObservable();
    }

    getUpdater(): Subject<User> {
        return this.updater;
    }

    login(email: string, password: string) {
        return this.api.post<any>(this.apiURL + '/auth/login', {email, password})
            .pipe(map(data => {
                localStorage.setItem('currentUser', JSON.stringify(data.user));
                localStorage.setItem('token', data.token);
                this.updater.next(data.user);

                return data;
            }));
    }

    logout() {
        localStorage.removeItem('token');
        localStorage.removeItem('currentUser');
        this.updater.next(null);
    }
}
