import {AfterViewInit, ChangeDetectorRef, Component, OnInit} from '@angular/core';
import {User} from '@app/models/user';
import {AuthService} from '@app/services/auth.service';

@Component({
    selector: 'app-main-navigation',
    templateUrl: './main-navigation.component.html',
    styleUrls: ['./main-navigation.component.scss']
})
export class MainNavigationComponent implements AfterViewInit,  OnInit {
    currentUser?: User;
    isLogged = false;

    constructor(
        private authService: AuthService,
        private cd: ChangeDetectorRef
    ) {
    }

    ngOnInit() {
        this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
        if (this.currentUser) {
            this.isLogged = true;
        }

        this.authService.isUserUpdated().subscribe(user => {
            this.currentUser = user;
            if (this.currentUser) {
                this.isLogged = true;
            } else {
                this.isLogged = false;
            }
        });
    }

    ngAfterViewInit() {
        this.cd.detectChanges();
    }
}
