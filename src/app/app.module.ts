import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {HttpClientModule, HTTP_INTERCEPTORS} from '@angular/common/http';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';

import {AppComponent} from './app.component';
import {LoginComponent} from './components/auth/login/login.component';
import {RegisterComponent} from './components/auth/register/register.component';
import {AppRoutingModule} from './app-routing.module';

import {JwtInterceptor} from './helpers/jwt.interceptor';
import {AuthService} from './services/auth.service';
import {UserService} from './services/user.service';
import {ChangePasswordService} from './services/change-password.service';

import {TodosComponent} from './components/todo/list/todos.component';
import {TodoCreateComponent} from './components/todo/create/todo-create.component';
import {TodoEditComponent} from './components/todo/edit/todo-edit.component';
import {TodoShowComponent} from './components/todo/show/todo-show.component';
import {ForgotPasswordComponent} from './components/auth/forgot-password/forgot-password.component';
import {ForgotPasswordChangeComponent} from './components/auth/forgot-password-change/forgot-password-change.component';
import {AuthGuard} from './guards/AuthGuard';
import {LogoutComponent} from './components/auth/logout/logout.component';
import {MainNavigationComponent} from './main-navigation/main-navigation.component';

@NgModule({
    declarations: [
        AppComponent,
        TodosComponent,
        LoginComponent,
        RegisterComponent,
        TodoCreateComponent,
        TodoEditComponent,
        TodoShowComponent,
        ForgotPasswordComponent,
        ForgotPasswordChangeComponent,
        LogoutComponent,
        MainNavigationComponent
    ],
    imports: [BrowserModule, HttpClientModule, AppRoutingModule, ReactiveFormsModule, FormsModule],
    providers: [
        AuthGuard,
        AuthService,
        UserService,
        ChangePasswordService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: JwtInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
