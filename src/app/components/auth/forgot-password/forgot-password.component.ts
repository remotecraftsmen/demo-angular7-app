import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {ChangePasswordService} from '@app/services/change-password.service';

@Component({
    selector: 'app-forgot-password',
    templateUrl: './forgot-password.component.html',
    styleUrls: ['./forgot-password.component.scss']
})
export class ForgotPasswordComponent implements OnInit {
    error: any = null;
    submitted = false;
    passwordSent = false;
    form: FormGroup;

    constructor(private changePasswordService: ChangePasswordService, private formBuilder: FormBuilder) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]]
        });
    }

    getField(name): string {
        return this.form.controls[name].value;
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            return;
        }

        this.changePasswordService.forgotPassword(this.getField('email')).subscribe(
            () => {
                this.passwordSent = true;
            },
            (error: HttpErrorResponse) => {
                if (error.status === 404) {
                    this.error = 'No user with the given email address';
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }
}
