import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {ChangePasswordService} from '@app/services/change-password.service';

@Component({
    selector: 'app-forgot-password-change',
    templateUrl: './forgot-password-change.component.html',
    styleUrls: ['./forgot-password-change.component.scss']
})
export class ForgotPasswordChangeComponent implements OnInit {
    submitted = false;
    error: any = null;
    token = '';
    passwordChanged = false;
    form: FormGroup;

    constructor(
        private changePasswordService: ChangePasswordService,
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            password: ['', Validators.required],
            password_confirmation: ['', Validators.required]
        });

        this.activatedRoute.params.subscribe((params: Params) => {
            this.token = params['token'];
        });
    }

    getField(name): string {
        return this.form.controls[name].value;
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            return;
        }

        this.changePasswordService.changePassword(this.token, this.getField('password'), this.getField('password_confirmation')).subscribe(
            () => {
                this.passwordChanged = true;
            },
            (error: HttpErrorResponse) => {
                console.log(error);

                if (error.error.status === 'error') {
                    this.error = error.error.message;
                }

                if (error.error.hasOwnProperty('errors')) {
                    this.error = '';
                    for (const err of error.error.errors) {
                        this.error += `<p>${err.param} ${err.message}</p>`;
                    }
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }
}
