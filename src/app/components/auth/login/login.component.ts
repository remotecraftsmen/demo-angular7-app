import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

import {AuthService} from '@app/services/auth.service';

@Component({
    selector: 'app-login',
    templateUrl: './login.component.html',
    styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
    submitted = false;
    authorized = true;
    form: FormGroup;
    errors = [];

    constructor(private authService: AuthService, private formBuilder: FormBuilder, private router: Router) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    getField(name): string {
        return this.form.controls[name].value;
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            return;
        }

        this.authService.login(this.getField('email'), this.getField('password')).subscribe(
            () => {
                return this.router.navigate(['/todos']);
            },
            (error: HttpErrorResponse) => {
                if (error.status === 401) {
                    this.authorized = false;
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }
}
