import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

import {UserService} from '@app/services/user.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
    submitted = false;
    form: FormGroup;
    errors = [];

    constructor(private userService: UserService, private formBuilder: FormBuilder, private router: Router) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            email: ['', [Validators.required, Validators.email]],
            username: ['', Validators.required],
            password: ['', Validators.required],
            password_confirmation: ['', Validators.required],
            first_name: ['', Validators.required],
            last_name: ['', Validators.required]
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            this.logErrors();
            return;
        }

        const user = this.form.value;

        this.userService.create(user).subscribe(
            () => {
                this.router.navigate(['/todos']).catch((e) => {
                    console.error(e);
                });
            },
            error => {
                console.log({error});

                if (error instanceof HttpErrorResponse) {
                    if (error.status === 400) {
                    }
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }

    // ! @TEMP:
    logErrors() {
        Object.keys(this.form.controls).forEach(key => {
            const errors = this.form.get(key).errors || [];

            Object.keys(errors).forEach(keyError => {
                console.log({
                    field: key,
                    error: keyError,
                    value: errors[keyError]
                });
            });
        });
    }
}
