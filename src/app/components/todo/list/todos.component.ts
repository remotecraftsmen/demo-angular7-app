import {Component, OnInit} from '@angular/core';
import {TodoService} from '@app/services/todo.service';
import {Todo} from '@app/models/todo';

@Component({
    selector: 'app-todos',
    templateUrl: './todos.component.html',
    styleUrls: ['./todos.component.scss']
})
export class TodosComponent implements OnInit {
    todos = [];

    constructor(private todoService: TodoService) {
    }

    ngOnInit() {
        this.todoService.getAll().subscribe((data: { todos: Todo[] }) => {
            this.todos = data.todos;
        });
    }
}
