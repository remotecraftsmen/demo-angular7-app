import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {TodoService} from '@app/services/todo.service';
import {Todo} from '@app/models/todo';

@Component({
    selector: 'app-todo-edit',
    templateUrl: './todo-edit.component.html',
    styleUrls: ['./todo-edit.component.scss']
})
export class TodoEditComponent implements OnInit {
    id: number;
    todo: Todo;
    form: FormGroup;
    submitted = false;

    constructor(
        private todoService: TodoService,
        private formBuilder: FormBuilder,
        private router: Router,
        private activatedRoute: ActivatedRoute
    ) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            user_id: [],
            completed: []
        });

        this.activatedRoute.params.subscribe((params: Params) => {
            this.id = params['id'];
        });

        this.todoService.get(this.id).subscribe((data: Todo) => {
            this.todo = data;
            this.setFormValues();
        });
    }

    setFormValues() {
        Object.keys(this.f).forEach(key => {
            this.f[key].setValue(this.todo[key]);
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            return;
        }

        this.todoService.update(this.id, this.form.value).subscribe(
            data => {
                this.router.navigate(['/todos']);
            },
            (error: HttpErrorResponse) => {
                if (error.status === 401) {
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }
}
