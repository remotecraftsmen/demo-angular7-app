import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from '@angular/router';

import {TodoService} from '@app/services/todo.service';

@Component({
    selector: 'app-todo-create',
    templateUrl: './todo-create.component.html',
    styleUrls: ['./todo-create.component.scss']
})
export class TodoCreateComponent implements OnInit {
    form: FormGroup;

    loading = false;
    submitted = false;

    constructor(private todoService: TodoService, private formBuilder: FormBuilder, private router: Router) {
    }

    ngOnInit() {
        this.form = this.formBuilder.group({
            name: ['', Validators.required],
            user_id: []
        });
    }

    onSubmit() {
        this.submitted = true;

        if (this.form.invalid) {
            return;
        }

        this.todoService.create(this.form.value).subscribe(
            data => {
                return this.router.navigate(['/todos']);
            },
            (error: HttpErrorResponse) => {
                if (error.status === 401) {
                }
            }
        );
    }

    get f(): any {
        return this.form.controls;
    }
}
