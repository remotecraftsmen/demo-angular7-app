import {Component, OnInit} from '@angular/core';
import {Router, ActivatedRoute, Params} from '@angular/router';

import {TodoService} from '@app/services/todo.service';
import {Todo} from '@app/models/todo';

@Component({
    selector: 'app-todo-show',
    templateUrl: './todo-show.component.html',
    styleUrls: ['./todo-show.component.scss']
})
export class TodoShowComponent implements OnInit {
    id: any = null;
    todo: Todo = new Todo();
    submitted = false;

    constructor(private todoService: TodoService, private activatedRoute: ActivatedRoute) {
        this.activatedRoute.params.subscribe((params: Params) => {
            this.id = params['id'];
        });
    }

    ngOnInit() {
        this.todoService.get(this.id).subscribe(data => {
            this.todo = <Todo>data;
        });
    }
}
