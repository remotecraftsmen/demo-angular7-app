export class Todo {
    id: number;
    name: string;
    user_id: number;
    creator_id: number;
    completed: boolean;
    created_at: string;
    updated_at: string;
}
