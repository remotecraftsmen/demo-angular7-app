import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './components/auth/login/login.component';
import { RegisterComponent } from './components/auth/register/register.component';
import { TodosComponent } from './components/todo/list/todos.component';
import { TodoCreateComponent } from './components/todo/create/todo-create.component';
import { TodoEditComponent } from './components/todo/edit/todo-edit.component';
import { TodoShowComponent } from './components/todo/show/todo-show.component';
import { ForgotPasswordComponent } from './components/auth/forgot-password/forgot-password.component';
import { ForgotPasswordChangeComponent } from './components/auth/forgot-password-change/forgot-password-change.component';
import { AuthGuard } from './guards/AuthGuard';
import { LogoutComponent } from './components/auth/logout/logout.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'logout', component: LogoutComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'forgot-password', component: ForgotPasswordComponent },
  { path: 'forgot-password/:token', component: ForgotPasswordChangeComponent },
  {
    path: 'todos', canActivate: [AuthGuard],
    children: [
      { path: '', component: TodosComponent },
      { path: 'create', component: TodoCreateComponent },
      { path: ':id', component: TodoShowComponent },
      { path: ':id/edit', component: TodoEditComponent }
    ]
  },
  // otherwise redirect to home
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
